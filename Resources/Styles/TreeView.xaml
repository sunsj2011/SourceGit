<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:converters="clr-namespace:SourceGit.Converters"
                    xmlns:helpers="clr-namespace:SourceGit.Helpers">

    <converters:TreeViewItemDepthToMargin x:Key="Converter.TreeViewItemIndent" Indent="19"/>

    <Style x:Key="Style.TreeView.ToggleButton" TargetType="{x:Type ToggleButton}">
        <Setter Property="Focusable" Value="False"/>
        <Setter Property="Width" Value="16" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="ToggleButton">
                    <Grid Width="16" Height="16" Margin="1" Background="Transparent">
                        <Path x:Name="ExpandPath" HorizontalAlignment="Left" VerticalAlignment="Center" Margin="1,1,1,1" Fill="{DynamicResource Brush.FG}" Data="M 4 0 L 8 4 L 4 8 Z"/>
                    </Grid>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsChecked" Value="True">
                            <Setter Property="Data" TargetName="ExpandPath" Value="M 0 4 L 8 4 L 4 8 Z"/>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="Style.TreeView.ItemContainerStyle" TargetType="{x:Type TreeViewItem}">
        <Setter Property="KeyboardNavigation.AcceptsReturn" Value="True" />
        <Setter Property="BorderThickness" Value="0"/>
        <Setter Property="FocusVisualStyle" Value="{x:Null}" />
        <Setter Property="HorizontalContentAlignment" Value="{Binding HorizontalContentAlignment, Mode=OneWay, FallbackValue=Stretch, RelativeSource={RelativeSource AncestorType={x:Type ItemsControl}}}" />
        <Setter Property="VerticalContentAlignment" Value="{Binding VerticalContentAlignment, Mode=OneWay, FallbackValue=Center, RelativeSource={RelativeSource AncestorType={x:Type ItemsControl}}}" />
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type TreeViewItem}">
                    <StackPanel>
                        <Border 
                            x:Name="BG" 
                            Background="Transparent" 
                            BorderThickness="0" 
                            Padding="{TemplateBinding Padding}"
                            SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}">
                            <Grid
                                Margin="{Binding Converter={StaticResource Converter.TreeViewItemIndent}, RelativeSource={x:Static RelativeSource.TemplatedParent}}"
                                VerticalAlignment="Stretch" 
                                Background="Transparent">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition/>
                                </Grid.ColumnDefinitions>
                                <ToggleButton 
                                    Grid.Column="0" 
                                    x:Name="Expander"
                                    Style="{StaticResource Style.TreeView.ToggleButton}"
                                    IsChecked="{Binding Path=IsExpanded, RelativeSource={x:Static RelativeSource.TemplatedParent}, Mode=TwoWay}"
                                    ClickMode="Press"/>
                                <ContentPresenter 
                                    x:Name="PART_Header" 
                                    Grid.Column="1" 
                                    VerticalAlignment="{TemplateBinding VerticalContentAlignment}" 
                                    HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                    ContentSource="Header"
                                    SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                            </Grid>
                        </Border>
                        <ItemsPresenter x:Name="ItemsHost" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                    </StackPanel>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsExpanded" Value="False">
                            <Setter TargetName="ItemsHost" Property="Visibility" Value="Collapsed"/>
                        </Trigger>
                        <Trigger Property="HasItems" Value="False">
                            <Setter TargetName="Expander" Property="Visibility" Value="Hidden"/>
                        </Trigger>
                        <Trigger Property="IsSelected" Value="True">
                            <Setter TargetName="BG" Property="Background" Value="{DynamicResource Brush.Accent1}"/>
                        </Trigger>
                        <MultiTrigger>
                            <MultiTrigger.Conditions>
                                <Condition SourceName="BG" Property="IsMouseOver" Value="True"/>
                                <Condition Property="IsSelected" Value="False"/>
                            </MultiTrigger.Conditions>
                            <Setter TargetName="BG" Property="Background" Value="{DynamicResource Brush.Accent2}"/>
                        </MultiTrigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="Style.TreeView.MultiSelectionItemContainerStyle" TargetType="{x:Type TreeViewItem}">
        <Setter Property="KeyboardNavigation.AcceptsReturn" Value="True" />
        <Setter Property="BorderThickness" Value="0"/>
        <Setter Property="FocusVisualStyle" Value="{x:Null}" />
        <Setter Property="HorizontalContentAlignment" Value="{Binding HorizontalContentAlignment, Mode=OneWay, FallbackValue=Stretch, RelativeSource={RelativeSource AncestorType={x:Type ItemsControl}}}" />
        <Setter Property="VerticalContentAlignment" Value="{Binding VerticalContentAlignment, Mode=OneWay, FallbackValue=Center, RelativeSource={RelativeSource AncestorType={x:Type ItemsControl}}}" />
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type TreeViewItem}">
                    <StackPanel>
                        <Border 
                            x:Name="BG" 
                            Background="Transparent" 
                            BorderThickness="0" 
                            Padding="{TemplateBinding Padding}"
                            SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}">
                            <Grid 
                                Margin="{Binding Converter={StaticResource Converter.TreeViewItemIndent}, RelativeSource={x:Static RelativeSource.TemplatedParent}}"
                                VerticalAlignment="Stretch" 
                                Background="Transparent">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition/>
                                </Grid.ColumnDefinitions>
                                <ToggleButton 
                                    Grid.Column="0" 
                                    x:Name="Expander"
                                    Style="{StaticResource Style.TreeView.ToggleButton}"
                                    IsChecked="{Binding Path=IsExpanded, RelativeSource={x:Static RelativeSource.TemplatedParent}, Mode=TwoWay}"
                                    ClickMode="Press"/>
                                <ContentPresenter 
                                    x:Name="PART_Header" 
                                    Grid.Column="1" 
                                    VerticalAlignment="{TemplateBinding VerticalContentAlignment}" 
                                    HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                    ContentSource="Header"
                                    SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                            </Grid>
                        </Border>
                        <ItemsPresenter x:Name="ItemsHost" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                    </StackPanel>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsExpanded" Value="False">
                            <Setter TargetName="ItemsHost" Property="Visibility" Value="Collapsed"/>
                        </Trigger>
                        <Trigger Property="HasItems" Value="False">
                            <Setter TargetName="Expander" Property="Visibility" Value="Hidden"/>
                        </Trigger>
                        <Trigger Property="helpers:TreeViewHelper.IsChecked" Value="True">
                            <Setter TargetName="BG" Property="Background" Value="{DynamicResource Brush.Accent1}"/>
                        </Trigger>
                        <MultiTrigger>
                            <MultiTrigger.Conditions>
                                <Condition SourceName="BG" Property="IsMouseOver" Value="True"/>
                                <Condition Property="helpers:TreeViewHelper.IsChecked" Value="False"/>
                            </MultiTrigger.Conditions>
                            <Setter TargetName="BG" Property="Background" Value="{DynamicResource Brush.Accent2}"/>
                        </MultiTrigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style TargetType="{x:Type TreeView}">
        <Setter Property="HorizontalContentAlignment" Value="Stretch" />
        <Setter Property="VerticalContentAlignment" Value="Center"/>
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="ItemContainerStyle" Value="{StaticResource Style.TreeView.ItemContainerStyle}"/>
        <Setter Property="VirtualizingStackPanel.IsVirtualizing" Value="True" />
        <Setter Property="VirtualizingStackPanel.VirtualizationMode" Value="Standard" />
        <Setter Property="ScrollViewer.CanContentScroll" Value="True" />
        <Setter Property="Background" Value="Transparent"/>
        <Setter Property="ItemsPanel">
            <Setter.Value>
                <ItemsPanelTemplate>
                    <VirtualizingStackPanel IsItemsHost="True"/>
                </ItemsPanelTemplate>
            </Setter.Value>
        </Setter>

        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type TreeView}">
                    <Border Name="Border"
                            Background="{TemplateBinding Background}"
                            BorderThickness="0"
                            CornerRadius="0"
                            SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}">
                        <ScrollViewer Padding="{TemplateBinding Padding}"
                                      CanContentScroll="{TemplateBinding ScrollViewer.CanContentScroll}"
                                      Focusable="False"
                                      HorizontalScrollBarVisibility="{TemplateBinding ScrollViewer.HorizontalScrollBarVisibility}"
                                      VerticalScrollBarVisibility="{TemplateBinding ScrollViewer.VerticalScrollBarVisibility}"
                                      SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}">
                            <ItemsPresenter SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"/>
                        </ScrollViewer>
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>

        <Style.Triggers>
            <Trigger Property="IsEnabled" Value="False">
                <Setter Property="Opacity" Value=".5"/>
            </Trigger>
        </Style.Triggers>
    </Style>
</ResourceDictionary>